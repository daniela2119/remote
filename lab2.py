class Train():
    def __init__(self, destination, number, departureTime, arrayOfFreePlaces):
        self.destination = destination
        self.number = number
        self.departureTime = departureTime
        self.numberOfGeneralPlaces = arrayOfFreePlaces[0]
        self.numberOfReservedPlaces = arrayOfFreePlaces[1]
        self.numberOfCompartmentPlaces = arrayOfFreePlaces[2]

train1 = Train("Киев", 191007, "12:34", [0, 0, 0])
train2 = Train("Днепр", 110807, "05:06", [67, 45, 22])
train3 = Train("Белая Церковь", 240512, "08:07", [78, 56, 22])
train4 = Train("Киев", 100906, "09:01", [12, 0, 12])
train5 = Train("Ивано-франковск", 110812, "23:45", [56, 34, 22])

group = [train1, train2, train3, train4, train5]

#код ниже выводит список поездов, следующих до заданного пункта назначения

destination = input("Введите пункт назначения: ")

for tr in group:
    if destination == tr.destination:
        print("Поезд рейса", tr.number, "отправляется в " + tr.destination + " в " + tr.departureTime)

#код ниже выводит список поездов, отправляющихся после заданного времени

time = input("Введите время отправки: ")
tHours = time[0:2]
tMinutes = time[3:]

for tr in group:
    hours = tr.departureTime[0:2]
    minutes = tr.departureTime[3:]
    if int(tHours) < int(hours):
        print("Поезд рейса", tr.number)
    elif int(tHours) == int(hours):
        if int(tMinutes) <= int(minutes):
            print("Поезд рейса ", tr.number)

#код ниже выводит список поездов, следующих до заданного пункта назначения и имеющих свободные общие места

destination = input("Введите пункт назначения: ")

for tr in group:
    if destination == tr.destination and tr.numberOfGeneralPlaces > 0:
        print("Поезд рейса", tr.number, "отправляется в " + tr.destination + " в " + tr.departureTime)