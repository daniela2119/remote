class GraphicElement():
    name = "графический элемент"
    leftTopCorner = "150:400"
    height = 30
    width = 10
    def __init__(self):
        print("done")
    def getLeftTopCorner(self):
        print(self.leftTopCorner)
    def getHeight(self):
        print(self.height)
    def getWidth(self):
        print(self.width)

class Button(GraphicElement):
    def __init__(self, title):
        self.title = title

class Slider(GraphicElement):
    def __init__(self, position):
        self.position = position

element1 = Button("Home")
element2 = Slider(80)

elements = [element1, element2]

for el in elements:
    el.getLeftTopCorner
    el.getHeight
    el.getWidth